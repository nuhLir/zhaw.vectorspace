var vectorSpace = function(){
	return {
		/**
		 * Retrieves the type for the specified modulo type and the two vectors.
		 *
		 * @param {Number} n The modulo number
		 * @param {vector2} first The first vector
		 * @param {vector2} second The second vector
		 *
		 * @returns {String} "frei" if it's a free room, otherwhise the 
		*/
		retrieveType: function(n, first, second){
			for (var i = 1; i < n; i++){
				for (var j = 1; j < n; j++){
					// check if result coressponds
					var result = first.scale(i).add(second.scale(j));

					// check if result is a zero vector for modulo
					if (result.x % n === 0 && result.y % n === 0){
						return i + "*" + first.toString() + " + " + j + "*"+second.toString()+" = "+(new vector2(0, 0).toString());
					}
				}
			}

			return 'frei';
		}
	};
};